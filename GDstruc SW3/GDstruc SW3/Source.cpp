#include <iostream>
#include <string>

using namespace std;

int addNums(int number)
{
	if (number == 0)
		return 0;

	else
     return(number % 10 + addNums(number / 10));
	
}

int fib(int x)
{
	if ((x == 1) || (x == 0))
	{
		return(x);
	}
	else 
	{
		return(fib(x - 1) + fib(x - 2));
	}
}

bool primecheck(int n, int i = 2)
{
	if (n <= 2)
	{
		return (n == 2) ? true : false;

	}

	if (n % i == 0)
		return false;

	if (i * i > n)
		return true;

	return primecheck(n, i + 1);
}

int main()
{
	int num;
	int x;
	int y;
	int r= 0;
	int i = 0;


	cout << "ENTER A NUMBER TO GET SUM OF DIGITS" << endl;
	cin >> num;

	cout << " SUM IS " << addNums(num) << endl;

	cout << "ENTER NUMBERS OF FIBONACCI SEQUENCE" << endl;
	cin >> x;


	while (i < x)
	{
		cout << " " << fib(i);
		i++;
	}

	cout << endl;

	cout << "ENTER A NUMBER TO CHECK IF IT IS PRIME OR NOT" << endl;
	cin >> y;

	if (primecheck(y))
	{
		cout << "IT IS A PRIME NUMBER" << endl;
	}

	else
	{
		cout << "IT IS NOT A PRIME NUMBER" << endl;
	}

	system("pause");
	
}